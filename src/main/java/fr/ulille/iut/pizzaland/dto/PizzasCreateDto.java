package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzasCreateDto {
	
	private String name;
	private List<Ingredient> ingredients;
		
	public PizzasCreateDto() {
		
	}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}
}
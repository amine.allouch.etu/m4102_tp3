package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizzas;

public interface PizzasDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzasTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pizzaID VARCHAR(128), ingredientID VARCHAR(128), PRIMARY KEY(pizzaID,ingredientID))")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzasTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropPizzasTable();

	@SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
	void insertPizzas(@BindBean Pizzas pizza);

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void removePizzas(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizzas.class)
	Pizzas findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizzas.class)
	List<Pizzas> getAll();

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizzas.class)
	Pizzas findById(@Bind("id") UUID id);

}